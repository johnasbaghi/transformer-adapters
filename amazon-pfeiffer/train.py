from datasets import load_dataset
from datasets import ClassLabel
from datasets import load_metric
from sklearn.metrics import f1_score
from transformers import RobertaTokenizer, TrainingArguments, EvalPrediction, Trainer, DataCollatorForLanguageModeling, DataCollatorForTokenClassification, DataCollatorWithPadding
from transformers import RobertaConfig, RobertaModelWithHeads
from transformers.adapters.configuration import AdapterConfig
import numpy as np
import torch
torch.cuda.empty_cache()

metric = load_metric('f1')
l = ClassLabel(num_classes=2, names_file="/home/ubuntu/code/transformer-adapters/amazon-pfeiffer/classlabels.txt")
tokenizer = RobertaTokenizer.from_pretrained("roberta-base")
#https://github.com/huggingface/notebooks/blob/master/examples/language_modeling.ipynb
# block_size = tokenizer.model_max_length
block_size = 512

# RuntimeError: The expanded size of the tensor (1024) must match the
# existing size (514) at non-singleton dimension 1.  Target sizes:
# [16, 1024].  Tensor sizes: [1, 514]

def tokenize_function(examples):
    return tokenizer(examples["text"])

def encode_batch(batch):
  """Encodes a batch of input data using the model tokenizer."""
  tokenized_batch  = tokenizer(batch['text'], max_length=1024, truncation=True, padding=True)
  tokenized_batch['label'] = l.str2int(batch['label'])
  return tokenized_batch #tokenizer(batch['text'], max_length=1024, truncation=True, padding=True)

def tokenize(batch):
  """Encodes a batch of input data using the model tokenizer."""
  tokenized_batch  = tokenizer(batch['text'], max_length=1024, truncation=True, padding=True)
  # batch_labels = tokenizer(batch['label'], max_length=1024, truncation=True, padding=True)
  return tokenizer(batch['text'], max_length=512, truncation=True, padding=True)

def encode_batch_label(batch):
  """Encodes a batch of input data using the model tokenizer."""
  return tokenizer(batch['label'], max_length=1024, truncation=True, padding="max_length")




# dataset = load_dataset('json', data_dir='.', data_files={'train': 'train.jsonl',
#                                                          'validation': 'dev.jsonl',
#                                                          'test': 'test.jsonl'})

dataset = load_dataset('json', data_dir='.', data_files={'train': 'data/train_128.jsonl',
                                                         'validation': 'data/dev_128.jsonl',
                                                         'test': 'data/test_128.jsonl'})

print(dataset.num_rows)
print(dataset['train'][0])
print(dataset['train'][1])

# tokenizer = RobertaTokenizer.from_pretrained("roberta-base")

dataset = dataset.map(encode_batch, batched=True)
dataset.rename_column_("label", "labels")

# Transform to pytorch tensors and only output the required columns
dataset.set_format(type='torch', columns=["input_ids", "attention_mask", "labels"])
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
config = RobertaConfig.from_pretrained(
    "roberta-base",
    num_labels=2,
)

# model = RobertaModelWithHeads.from_pretrained(
#     "roberta-base",
#     config=config,
# ).to('cuda:0')

model = RobertaModelWithHeads.from_pretrained("roberta-base", config=config).to('cuda:0')

#https://docs.adapterhub.ml/classes/adapter_config.html
#adapterconfig = AdapterConfig.load('houlsby')
adapterconfig = AdapterConfig.load('pfeiffer')

model.add_adapter("amazon", config=adapterconfig)

model.add_classification_head(
    "amazon",
    num_labels=2
  )
model.train_adapter("amazon")

training_args = TrainingArguments(
    learning_rate=1e-4,
    num_train_epochs=10,
    per_device_train_batch_size=16,
    per_device_eval_batch_size=16,
    logging_steps=200,
    output_dir="./training_output",
    overwrite_output_dir=True,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  labels = p.label_ids
  print('Accuracy: ', (preds == p.label_ids).mean())
  print('f1 score: ', f1_score(labels, preds, average='micro'))
  return {"acc": (preds == p.label_ids).mean()}
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["validation"],
    data_collator=data_collator,
    compute_metrics=compute_accuracy,
)

# trainer.train()
# trainer.evaluate()

if checkpoint is not None:
    trainer.train(resume_from_checkpoint=checkpoint)
else:
    trainer.train()
    trainer.evaluate()
    trainer.predict(dataset["test"])
