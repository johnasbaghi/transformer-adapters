# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python Docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# Input data files are available in the read-only "../input/" directory
# For example, running this (by clicking run or pressing Shift+Enter) will list all files under the input directory

import os
for dirname, _, filenames in os.walk('/kaggle/input'):
    for filename in filenames:
        print(os.path.join(dirname, filename))

# You can write up to 20GB to the current directory (/kaggle/working/) that gets preserved as output when you create a version using "Save & Run All"

# You can also write temporary files to /kaggle/temp/, but they won't be saved outside of the current session

# !pip install -q --force-reinstall transformers
# !pip install -q --force-reinstall git+https://github.com/adapter-hub/adapter-transformers.git
!pip install adapter-transformers
# install datasets
!pip install -q datasets

# Make sure that we have a recent version of pyarrow in the session before we continue - otherwise reboot Colab to activate it
import pyarrow
if int(pyarrow.__version__.split('.')[1]) < 16 and int(pyarrow.__version__.split('.')[0]) == 0:
        import os
            os.kill(os.getpid(), 9)

            #!git clone https://github.com/huggingface/transformers
            #!python transformers/utils/download_glue_data.py --tasks SST



            import numpy as np
            import pandas as pd
            from sklearn import metrics
            import transformers
            import torch
            import dataclasses
            import logging
            import os
            import sys
            from dataclasses import dataclass, field
            from typing import Dict, Optional

            from torch.utils.data import Dataset, DataLoader, RandomSampler, SequentialSampler
            from transformers import BertTokenizer, BertModel, BertConfig
            from transformers import RobertaTokenizer, RobertaModel
            from transformers import AutoTokenizer, EvalPrediction, AutoModelWithHeads, AdapterType
