import json
import pdb
import os

def get_lines(file_path='test.jsonl'):
    handle = open(file_path, 'r+', encoding="utf-8")
    lines = handle.readlines()
    lines = [line.rstrip() for line in lines]
    return lines


def print_labels():
    lines = get_lines()
    test_data = []

    for line in lines:
        data = json.loads(line)
        test_data.append(data)
        pdb.set_trace()
        print(data['label'])


def append_file(text, file_path):
    with open(file_path, "a") as myfile:
        myfile.write(text+"\n")



def delete_file(file_path):
    if not os.path.exists(file_path):
        return
    
    try:
        os.remove(file_path)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred


def truncate_text(in_file_path, out_file_path, new_size=512):
    delete_file(out_file_path)
    
    lines = get_lines(in_file_path)
    test_data = []

    new_data = {}
    
    for line in lines:
        data = json.loads(line)

        new_data['id'] = data['id']
        new_data['label'] = data['label']
        new_data['text'] = data['text'][0:new_size]

        append_file(json.dumps(new_data), out_file_path)


truncate_text(in_file_path='test.jsonl', out_file_path='test_512.jsonl', new_size=512)
truncate_text(in_file_path='dev.jsonl', out_file_path='dev_512.jsonl', new_size=512)
truncate_text(in_file_path='train.jsonl', out_file_path='train_512.jsonl', new_size=512)
