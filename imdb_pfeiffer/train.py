import pdb
import numpy as np
from datasets import load_dataset
from transformers import RobertaTokenizer
from transformers import RobertaConfig, RobertaModelWithHeads
from transformers import TrainingArguments, Trainer, EvalPrediction
from transformers import TextClassificationPipeline
from transformers.adapters.configuration import AdapterConfig
from sklearn.metrics import f1_score
import torch
torch.cuda.empty_cache()

dataset = load_dataset('json', data_dir='.', data_files={'train': 'data/train.jsonl',
                                                         'validation': 'data/dev.jsonl',
                                                         'test': 'data/test.jsonl'})

tokenizer = RobertaTokenizer.from_pretrained("roberta-base")

def encode_batch(batch):
    """Encodes a batch of input data using the model tokenizer."""
    return tokenizer(batch["text"], max_length=80, truncation=True, padding="max_length")

# Encode the input data
dataset = dataset.map(encode_batch, batched=True)

# The transformers model expects the target class column to be named "labels"
dataset.rename_column_("label", "labels")

# Transform to pytorch tensors and only output the required columns
dataset.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])

config = RobertaConfig.from_pretrained("roberta-base", num_labels=2,)

#model = RobertaModelWithHeads.from_pretrained("roberta-base", config=config,)
model = RobertaModelWithHeads.from_pretrained("roberta-base", config=config).to('cuda:0')

# Add a new adapter
# adapter_config = AdapterConfig.load('houlsby')
adapter_config = AdapterConfig.load('pfeiffer')
model.add_adapter("imdb", config=adapter_config)

# Add a matching classification head
model.add_classification_head(
    "imdb",
    num_labels=2,
    id2label={ 0: "👎", 1: "👍"}
  )

# Activate the adapter
model.train_adapter("imdb")

training_args = TrainingArguments(
    learning_rate=1e-4,
    num_train_epochs=20,
    per_device_train_batch_size=32,
    per_device_eval_batch_size=32,
    logging_steps=200,
    output_dir="./training_output",
    overwrite_output_dir=True,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  labels = p.label_ids
  print('Accuracy: ', (preds == p.label_ids).mean())
  print('f1 score: ', f1_score(labels, preds, average='micro'))
  return {"acc": (preds == p.label_ids).mean()}

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["validation"],
    compute_metrics=compute_accuracy,
)

trainer.train()

trainer.evaluate()
