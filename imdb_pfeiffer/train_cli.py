from datasets import load_dataset
from datasets import ClassLabel
from datasets import load_metric
from sklearn.metrics import f1_score
from transformers import RobertaTokenizer, TrainingArguments, EvalPrediction, Trainer, \
    DataCollatorForLanguageModeling, DataCollatorForTokenClassification, DataCollatorWithPadding, AdapterTrainer, \
    EarlyStoppingCallback
from transformers import RobertaConfig, RobertaModelWithHeads
from transformers.adapters.configuration import AdapterConfig
import numpy as np
import torch
import argparse
torch.cuda.empty_cache()

def get_classes(adapter):
    #directory with class labels and datasets
    file = adapter + '/labels.txt'
    num_classes = len(open(file).readlines())
    classlabels = ClassLabel(num_classes=num_classes, names_file=file)
    return classlabels, num_classes

parser = argparse.ArgumentParser(description='Adapter model')
parser.add_argument('--adapter_model', help='Adapter model to use', default='ChemProt')
parser.add_argument('--learning_rate', help='Learning Rate', default=1e-4)
parser.add_argument('--num_epochs', help='Number of Epochs', default=50)
parser.add_argument('--batch_size', help='Batch Size', default=16)
parser.add_argument('--embedding_length', help='Language Model Embedding Length', default=512)
parser.add_argument('--checkpoint', help='Load previous model weights', default=None)
parser.add_argument('--early_stopping_patience', help='Number of steps without improving before stopping',default=15)
args = parser.parse_args()
adapter = args.adapter_model
learning_rate = args.learning_rate
num_epochs = args.num_epochs
batch_size = args.batch_size
embed_length = args.embedding_length
checkpoint = args.checkpoint
early_stopping_patience = args.early_stopping_patience

classlabels, num_classes = get_classes(adapter)
tokenizer = RobertaTokenizer.from_pretrained("roberta-base")

def encode_batch(batch):
  """Encodes a batch of input data using the model tokenizer."""
  tokenized_batch  = tokenizer(batch['text'], max_length=embed_length, truncation=True, padding=True)
  tokenized_batch['label'] = classlabels.str2int(batch['label'])
  return tokenized_batch


dataset = load_dataset('json', data_dir='.', data_files={'train': adapter + '/train.jsonl',
                                                         'validation': adapter + '/dev.jsonl',
                                                         'test': adapter + '/test.jsonl'})

dataset = dataset.map(encode_batch, batched=True)
dataset.rename_column_("label", "labels")
# Transform to pytorch tensors and only output the required columns
dataset.set_format(type='torch', columns=["input_ids", "attention_mask", "labels"])
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

config = RobertaConfig.from_pretrained("roberta-base",num_labels=num_classes)

model = RobertaModelWithHeads.from_pretrained("roberta-base", config=config).to('cuda:0')
#https://docs.adapterhub.ml/classes/adapter_config.html
adapterconfig = AdapterConfig.load('houlsby')
model.add_adapter(adapter, config=adapterconfig)

model.add_classification_head(adapter,num_labels=num_classes)
model.train_adapter(adapter)

training_args = TrainingArguments(
    learning_rate=learning_rate,
    num_train_epochs=num_epochs,
    #https://stackoverflow.com/questions/69087044/early-stopping-in-bert-trainer-instances
    evaluation_strategy='steps',
    eval_steps=50, #hardcoded for now
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    logging_steps=200,
    output_dir="./" + adapter + "/training_output",
    overwrite_output_dir=True,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
    metric_for_best_model='eval_f1',
    load_best_model_at_end=True,
    save_total_limit = 5 #only keep most recent weights
)

def compute_accuracy(p: EvalPrediction):
  preds = np.argmax(p.predictions, axis=1)
  labels = p.label_ids
  print('Accuracy: ', (preds == p.label_ids).mean())
  print('f1 score: ', f1_score(labels, preds, average='micro'))
  #https://discuss.huggingface.co/t/early-stopping-callback-problem/5649
  return {'eval_f1':f1_score(labels, preds, average='micro'), 'eval_Accuracy':(preds == p.label_ids).mean()}
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["validation"],
    data_collator=data_collator,
    compute_metrics=compute_accuracy,
    #https://stackoverflow.com/questions/69087044/early-stopping-in-bert-trainer-instances
    #https://discuss.huggingface.co/t/early-stopping-callback-problem/5649
    callbacks = [EarlyStoppingCallback(early_stopping_patience=early_stopping_patience)]
)
if checkpoint is not None:
    trainer.train(resume_from_checkpoint=checkpoint)
else:
    trainer.train()
trainer.evaluate()

