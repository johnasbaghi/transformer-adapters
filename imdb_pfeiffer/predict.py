# Note: This code is copied from the following sources:
# https://colab.research.google.com/github/Adapter-Hub/adapter-transformers/blob/master/notebooks/01_Adapter_Training.ipynb
# https://github.com/Adapter-Hub/adapter-transformers

import numpy as np
from datasets import load_dataset
from transformers import RobertaTokenizer
from transformers import RobertaConfig, RobertaModelWithHeads
from transformers import TrainingArguments, Trainer, EvalPrediction
from transformers import TextClassificationPipeline

config = RobertaConfig.from_pretrained("roberta-base", num_labels=2,)

model = RobertaModelWithHeads.from_pretrained("roberta-base", config=config1)
model.load_adapter("./final_adapter")
model.set_active_adapters("rotten_tomatoes")

tokenizer = RobertaTokenizer.from_pretrained("roberta-base")

training_args = TrainingArguments(
    do_train=False,
    learning_rate=1e-4,
    num_train_epochs=6,
    per_device_train_batch_size=32,
    per_device_eval_batch_size=32,
    logging_steps=200,
    output_dir="./training_output",
    overwrite_output_dir=True,
    # The next line is important to ensure the dataset labels are properly passed to the model
    remove_unused_columns=False,
)

classifier = TextClassificationPipeline(model=model1, tokenizer=tokenizer)

classifier("This is awesome!")
